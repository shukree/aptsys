<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('apartments', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->unsignedBigInteger('city_id');
            $table->unsignedBigInteger('owner_id');
            $table->timestamps();

            $table->foreign('city_id')->references('id')->on('cities')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');

            $table->foreign('owner_id')->references('id')->on('users')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apartments');
    }
}
