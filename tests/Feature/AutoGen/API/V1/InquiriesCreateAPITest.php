<?php

namespace Tests\Feature\AutoGen\API\V1;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class InquiriesCreateAPITest extends APIBaseTestCase
{

	use DatabaseTransactions;

    /**
     *
     * 
     *
     * @return  void
     */
    public function test_api_inquiries_post_create()
    {
        $data = $cookies = $files = $headers = $server = [];
        $faker = \Faker\Factory::create('en_AU');
        $content = null;

					// header params
	        	            	                    $headers['Accept'] = 'application/json';
	            	        	            	                    $headers['x-access-token'] = $this->getAccessToken();
	                    	        	            	                    $headers['x-api-key'] = $this->getApiKey();
	                    	        		
					// form params
                            $data['title'] = '';
                            $data['description'] = '';
                            $data['code'] = '';
                            $data['user_id'] = '';
                            $data['apartment_id'] = '';
            		
                        $response = $this->post('/api/v1/inquiries', $data, $headers);
                
        $this->saveResponse($response->getContent(), 'inquiries_post_create', $response->getStatusCode());

		$response->assertStatus(200);
    }

}
