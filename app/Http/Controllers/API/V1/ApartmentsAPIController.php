<?php

namespace App\Http\Controllers\API\V1;

use App\Entities\Apartments\Apartment;
use App\Entities\Apartments\ApartmentsRepository;
use App\Http\Controllers\API\V1\APIBaseController;
use EMedia\Api\Docs\APICall;
use EMedia\Api\Docs\Param;
use Illuminate\Http\Request;
use Throwable;

class ApartmentsAPIController extends APIBaseController
{

	protected $repo;

	public function __construct(ApartmentsRepository $repo)
	{
		$this->repo = $repo;
	}

	protected function index(Request $request)
	{
		document(function () {
			return (new APICall())
			  ->setGroup('Apartments')
			  ->setName('Get Apartments')
			  ->noDefaultHeaders()
			  ->setHeaders([
				(new Param('Accept', 'String', '`application/json`'))->setDefaultValue('application/json'),
				(new Param('x-api-key', 'String', 'API Key'))->setDefaultValue('123-123-123-123'),
				(new Param('x-access-token', 'String', 'Access Token'))->setDefaultValue('123-123-123-123'),
				])
			  ->setParams([
				(new Param('q', 'String', 'Search query'))->optional(),
				(new Param('page', 'String', 'Number of the page to retrieve'))->setDefaultValue('1'),
				(new Param('city_id', 'String', 'Search query'))->optional(),
			  ])
			  ->setSuccessPaginatedObject(Apartment::class);
		  });

		
		$items = $this->repo->getFiltered($request)->paginate((int) $request->input('per_page', 10));
		return response()->apiSuccessPaginated($items);
	}

}
