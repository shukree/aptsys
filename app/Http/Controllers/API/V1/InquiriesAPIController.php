<?php

namespace App\Http\Controllers\API\V1;

use App\Entities\Inquiries\InquiriesRepository;
use App\Entities\Inquiries\Inquiry;
use App\Http\Controllers\API\V1\APIBaseController;
use EMedia\Api\Docs\APICall;
use EMedia\Api\Docs\Param;
use Illuminate\Http\Request;

class InquiriesAPIController extends APIBaseController
{

	protected $repo;

	public function __construct(InquiriesRepository $repo)
	{
		$this->repo = $repo;
	}

	protected function index(Request $request)
	{
		document(function () {
                	return (new APICall())
                	    ->setParams([
                	        'q|Search query',
                	        'page|Page number',
                        ])
                        ->setSuccessPaginatedObject(Inquiry::class);
                });

		$items = $this->repo->search();

		return response()->apiSuccess($items);
	}

	protected function store(Request $request)
	{
		document(function () {
            return (new APICall)
				->setGroup('Inquiries')
                ->setName('Create')
                ->setParams([
                    (new Param('title', 'String', 'Title')),
                    (new Param('description', 'String', 'Inquiry details')),
                    (new Param('code', 'String', 'N/A')),
					(new Param('user_id', 'Integer', 'User who made inquiry')),
					(new Param('apartment_id', 'Integer', 'Apartment related to inquiry')),
                ])
                ->noDefaultHeaders()
                ->setHeaders([
                    (new Param('Accept', 'String', '`application/json`'))->setDefaultValue('application/json'),
                    (new Param('x-api-key', 'String', 'API Key'))->setDefaultValue('123-123-123-123'),
                    (new Param('x-access-token', 'String', 'Access Token'))->setDefaultValue('123-123-123-123'),
                ])
                ->setSuccessObject(Inquiry::class)
                ->setSuccessExample('{
                    "payload": null,
                    "message": "Your inquiry has been sent",
                    "result": true
                }');
        });

		$this->validate($request,[
			'title'			=> 'required',
			'description'	=> 'required',
			'user_id'		=> 'required',
			'apartment_id'	=> 'required',
		]);

		$inquiry = new Inquiry();
		$inquiry->title = $request->input('title');
		$inquiry->description = $request->input('description');
		$inquiry->code = $request->input('code');
		$inquiry->user_id = $request->input('user_id');
		$inquiry->apartment_id = $request->input('apartment_id');
		
		$inquiry->save();

		if(!$inquiry)
		{
			return response()->apiError('Could not make inquiry');
		}

		return response()->apiSuccess(null, 'Your inquiry has been sent'); 
	}

}
