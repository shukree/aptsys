<?php

namespace App\Entities\Apartments;

use App\Entities\Inquiries\Inquiry;
use App\Entities\Cities\City;
use EMedia\Formation\Entities\GeneratesFields;
use ElegantMedia\SimpleRepository\Search\Eloquent\SearchableLike;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Apartment extends Model
{

    use HasFactory;
	use SearchableLike;
	use GeneratesFields;

	// use \Cviebrock\EloquentSluggable\Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    /*
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    */

	protected $fillable = [
		'name',
        'description',
        'city_id',
        'owner_id',
	];

	protected $searchable = [
		'name',
        'city_id',
        'owner_id',
	];

	protected $editable = [
    	'name',
        'description',
        [
            'name' => 'city_id',
            'display_name' => 'City',
            'type' => 'select',
            'options_entity' => 'App\Entities\Cities\City',
        ],
        [
            'name' => 'owner_id',
            'display_name' => 'Owner',
            'type' => 'select',
            'options_entity' => 'App\Models\User',
        ],
    ];

    /**
     *
     * Add any update only validation rules for this model
     *
     * @return array
     */
    public function getCreateRules()
    {
        return [
            'name' => 'required',
        ];
    }

    /**
     *
     * Add any update only validation rules for this model
     *
     * @return array
     */
    public function getUpdateRules()
    {
        return [
            'name' => 'required',
        ];
    }

    /**
     *
     * Add any update only validation messations
     *
     * @return array
     */
    public function getCreateValidationMessages()
    {
        return [];
    }

    /**
     *
     * Add any update only validation messations
     *
     * @return array
     */
    public function getUpdateValidationMessages()
    {
        return [];
    }

    public function inquiry()
	{
		return $this->hasMany(Inquiry::class);
	}

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }


}
