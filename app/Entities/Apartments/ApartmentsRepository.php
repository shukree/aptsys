<?php

namespace App\Entities\Apartments;

use App\Entities\BaseRepository;

class ApartmentsRepository extends BaseRepository
{

	public function __construct(Apartment $model)
	{
		parent::__construct($model);
	}

	public function getFiltered($request)
	{
		$query = Apartment::query();

		if($request->has('q'))
		{
			$query->where('name', 'like', '%'.$request->q.'%');
		}

		if($request->has('city_id'))
		{
			$query->where('city_id' , $request->city_id);
		}

		return $query;
	}

}
